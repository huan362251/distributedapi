package user;

public interface UserService {

    UserDTO queryUser(String id);

    void saveUser(UserDTO userDTO);

}
